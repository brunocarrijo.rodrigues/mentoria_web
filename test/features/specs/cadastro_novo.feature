#language: pt
@all
Funcionalidade: Cadastrar Novo usuário
  Sendo um usuário nao cadastrado
  Posso efetuar meu cadastro
  Para acessar o site e efetuar comprar
  Contexto: Acessar home
    Dado que acesso a home do MyStore
  @cadastro
  Cenario: Cadastro com sucesso
    Quando acesso a tela de cadastro 
    Então ao finalizar o preenchimento, meu cadastro estará completo