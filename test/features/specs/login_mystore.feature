#language: pt
@all
Funcionalidade: Login MyStore
  Sendo um usuário
  Posso fazer uma pesquisa no myStore
  Para uso pessoal ou para presente
  Contexto: Acessar home
    Dado que acesso a home do MyStore
  @acesso
  Cenario: Login com sucesso
    Quando acesso o login com os dados
    Então apresenta "Your account" na pagina logada