class AcessoSucesso < SitePrism::Page
    set_url ' '
    def login_acesso(login, passw)
        find(ELL['btn_singin']).click
        find(ELL['cmp_login']).set login
        find(ELL['cmp_senha']).set passw
        find(ELL['btn_subm']).click
    end    
end