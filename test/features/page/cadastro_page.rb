class CadastroCompletpo < SitePrism::Page
    set_url ''
    def cadastro_novo
        find(ELL['btn_singin']).click
        find(ELL['lnk_create']).click
        page.execute_script("$('#customer-form>section>div:nth-child(1)>div.col-md-6.form-control-valign>label:nth-child(1)').click();")
        find(ELL['name']).set Faker::Name.first_name
        find(ELL['last']).set Faker::Name.last_name
        @login = Faker::Internet.email
        find(ELL['e_mail']).set @login
        LOADS['login']<< @login
        find(ELL['niver']).set Faker::Date.birthday(min_age: 18, max_age: 65)
        @senha = '123456'
        find(ELL['passw']).set @senha
        LOADS['senha'] << @senha
        page.execute_script("$('#customer-form > section > div:nth-child(9) > div.col-md-6 > span > label > input[type=checkbox]').click();")
        find(ELL['btn_save']).click
    end

    def log_out 
        find(ELL['out']).click
    end
    
end