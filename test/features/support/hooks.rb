After do |scenario|
    scenario_name = scenario.name.gsub(/[^\w\-]/, ' ')
    if scenario.failed?
        take_a_picture(scenario_name.downcase!, 'Falhou')
    else
        take_a_picture(scenario_name.downcase!, 'Sucesso')
    end
end

def take_a_picture(file_name, resultado)
    data = Time.now.strftime('%F').to_s
    h_m_s = Time.now.strftime('%H%M%S%p').to_s
    temp_shot = page.save_screenshot("results/evidencia/#{data}/temp_#{h_m_s}.png")

    attach(temp_shot, 'image/png')
end

def screenshot()
    data = Time.now.strftime('%F').to_s
    h_m_s = Time.now.strftime('%H%M%S%p').to_s
    temp_shot = page.save_screenshot("results/evidencia/#{data}/step/temp_#{h_m_s}.png")

    attach(temp_shot, 'image/png')
end

Before do
    login_page.load
    cadastro_page.cadastro_novo
    cadastro_page.log_out 
end