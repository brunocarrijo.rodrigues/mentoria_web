Quando('acesso a tela de cadastro') do
    cadastro_page.load
    @cadastro.cadastro_novo
end

Então('ao finalizar o preenchimento, meu cadastro estará completo') do
    expect(page).to have_content "Sign out"
    screenshot
end